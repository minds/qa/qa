# Minds QA

The current QA process consists of both [manual](https://minds.tenant.kiwitcms.org/) and [automated](https://gitlab.com/minds/front/tree/cypress) tests. 

## Merge Requests Approvals

Before merge requests are merged into master, they should complete a smoke test. Merge requests ready to be merged should be tagged with the ~"MR::Ready to Merge" label.

### QA Testers

| Username | List |
| ------ | ------ |
| @xander-miller | [Click here](https://gitlab.com/groups/minds/-/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&approver_usernames[]=xander-miller&wip=no&label_name[]=Status%3A%3AReady%20to%20Merge) |

## Staging

**Master** will automatically be deployed to the staging environment. Following this, the automated e2e tests will run. Once the manual qa tests have been performed, the `manual:qa` job should be run/played. 

![Manual QA Screenshot](assets/manual-qa.png?raw=true "Manual QA Step")

| Repository | Link |
| ------ | ------ |
| engine | https://gitlab.com/minds/engine/environments/777308 |
| front | https://gitlab.com/minds/front/environments/779466 | 

## Regressions

Regressions found during the QA process in **merge requests** should be left as comments on the respective MR. 

Regressions found during the QA process on **staging** should be created as issues and labeled with ~"Type::Regression" and ~"Regression::Staging" and assigned to the relevant developer. Labels also exists for ~"Regression::Canary" and ~"Regression::Production".
